import react from 'react';
import './style.css';

const Top = () => {
  return (
    <div className='top'>
      <div className='top__right'>
        <span href="#" className='top__text'>FACE MASKS - BUY 1, GET 1 FOR 50% OFF
</span>
      </div>
      <div className='top__left'>
        <svg className='top__icon'>
          <svg id="usp-delivery" viewBox="0 0 19 19"><title>usp-delivery</title><g fill="none" stroke="currentColor" stroke-miterlimit="10"><path d="M13.42 13.5H9.5"></path><path stroke-linecap="square" d="M4.5 5.5h10l4 3v5h-2m-10 0h-2m0-6h-4"></path><circle cx="8" cy="13" r="1.5"></circle><circle cx="15" cy="13" r="1.5"></circle><path stroke-linecap="square" d="M1.5 9.5h3m-2 2h2"></path></g></svg>
        </svg>
        <span href="#" className='top__text'>FREE SHIPPING & RETURNS | JOIN NOW</span>
      </div>
    </div>
  )
}

export default Top;