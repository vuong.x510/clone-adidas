import React from 'react';
import App from './App.js';
import ReactDOM from 'react-dom';

import './fonts/729-font.ttf'; 
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
